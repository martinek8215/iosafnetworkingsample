//
//  ResultViewController.h
//  iOS_WebService
//
//  Created by Adrian on 10/11/15.
//  Copyright © 2015 Adrian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController

@property (strong, nonatomic) NSString * strEmail;
@property (strong, nonatomic) NSString * strUsername;
@property (strong, nonatomic) NSString * strGuidCode;

@end
