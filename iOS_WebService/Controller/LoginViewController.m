//
//  ViewController.m
//  iOS_WebService
//
//  Created by Adrian on 09/11/15.
//  Copyright (c) 2015 Adrian. All rights reserved.
//

#import "CTConfig.h"
#import "LoginViewController.h"
#import "NetAPIClient.h"
#import "CodeInputViewController.h"
#import <SVProgressHUD.h>

@interface LoginViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
//@property (strong, nonatomic) NSString * strCode;
//@property (strong, nonatomic) NSString * strEmail;
//@property (strong, nonatomic) NSString * strUsername;
@property (strong, nonatomic) NSString * strMessage;
@property (assign, nonatomic) BOOL isVerified;
@property (strong, nonatomic) NetAPIClient * netClient;
@end

@implementation LoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _netClient = [[NetAPIClient alloc] init];
}


#pragma mark - UITextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Actions

- (IBAction)tapBtnDone:(id)sender {
    void (^successed)(id responseObject) = ^(id responseObject) {
        NSInteger code = [[responseObject objectForKey:@"code"] integerValue];
        
        [SVProgressHUD dismiss];
        
        if (code == SUCCESS_SEND_GUIDCODE) {
            _strMessage = @"Please check your email to get GUID code";
//            _strCode = [responseObject objectForKey:@"guid"];
            gStrCode = [responseObject objectForKey:@"guid"];
            [self performSegueWithIdentifier:@"showCodeInputView" sender:nil];
        }
        else if(code == SUCCESS_SEND_VERIFICATIONCODE) {
            _lblMessage.text = @"Your email is not verified. Please verify your email at the first";
        }
    };
    
    void (^failure)(NSError* error) = ^(NSError* error) {
        _lblMessage.text = @"Sign in failed due to internet connection problem. Please retry.";
    };
    
//    _strEmail = [_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    _strUsername = [_txtUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    gStrEmail = [_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    gStrUsername = [_txtUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([gStrEmail isEqualToString:@""] || [gStrUsername isEqualToString:@""]) {
        _lblMessage.text = @"Please input valid email address and user name";
    }
    
    [SVProgressHUD showWithStatus:@"Login..."];
    
    NSMutableDictionary * params  = [NSMutableDictionary dictionary];
    [params setObject:gStrEmail forKey:@"email"];
    [params setObject:gStrUsername forKey:@"username"];
    
    [_netClient sendToServiceByPOST:WEBAPI_URL
                         controller:WEBAPI_ACCOUNT_CONTROLLER
                             action:WEBAPI_USER_SIGNIN
                             params:params
                            success:successed
                            failure:failure];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showCodeInputView"]) {
        CodeInputViewController * vc = [segue destinationViewController];
//        vc.strCode = _strCode;
//        vc.strUsername = _strUsername;
//        vc.strEmail = _strEmail;
        vc.strMessage = @"Please check your email to get the GUID code";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
