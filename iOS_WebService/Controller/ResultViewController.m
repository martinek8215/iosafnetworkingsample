//
//  ResultViewController.m
//  iOS_WebService
//
//  Created by Adrian on 10/11/15.
//  Copyright © 2015 Adrian. All rights reserved.
//

#import <sqlite3.h>
#import "ResultViewController.h"
#import "CTConfig.h"

@interface ResultViewController()

@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblGuidCode;

@end

@implementation ResultViewController

- (void)viewDidLoad
{
    _lblEmail.text = gStrEmail;
    _lblUsername.text = gStrUsername;
    _lblGuidCode.text = gStrCode;
}

@end
