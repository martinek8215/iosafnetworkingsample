//
//  CodeInputViewController.m
//  iOS_WebService
//
//  Created by Adrian on 10/11/15.
//  Copyright © 2015 Adrian. All rights reserved.
//

#import <sqlite3.h>
#import "CodeInputViewController.h"
#import "CTConfig.h"
#import <SVProgressHUD.h>

@interface CodeInputViewController()
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UITextField *txtGuidCode;
@property sqlite3 * local_db;

@end

@implementation CodeInputViewController

- (void)viewdidload
{
    _lblMessage.text = _strMessage;
}


#pragma mark - Actions

- (IBAction)tapBtnDone:(id)sender {
    NSString * code = [_txtGuidCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([code isEqualToString:gStrCode]) {
        
        //Save to Sqlite
        
        // Get the documents directory
        NSArray * dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * docsDir = [dirPaths objectAtIndex:0];
        NSString * databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"local.db"]];
        
        NSFileManager * filemgr = [NSFileManager defaultManager];
        const char * dbpath = [databasePath UTF8String];
        
        char *errMsg;
        
        if ([filemgr fileExistsAtPath: databasePath ] == NO) {
            if (sqlite3_open(dbpath, &_local_db) == SQLITE_OK) {

                const char *sql_stmt_history  = "create table practice(email char(50), username char(100), guid int)";
                if (sqlite3_exec(_local_db, sql_stmt_history, NULL, NULL, &errMsg) != SQLITE_OK) {
                    NSLog(@"Fail to crate table");
                }
            }
            else {
                NSLog( @"Failed to open/create database" );
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"ERROR!" message:@"Can't create database" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
            }
        }

        if (sqlite3_open(dbpath, &_local_db) == SQLITE_OK) {
            sqlite3_stmt * stmt;
            int rows;
            NSString * selectSQL = [NSString stringWithFormat:@"select count(*) from practice where email='%@' and username='%@';", gStrEmail, gStrUsername];
            
            if (sqlite3_prepare_v2(_local_db, [selectSQL UTF8String], -1, &stmt, NULL) == SQLITE_OK)
            {
                if (sqlite3_step(stmt) == SQLITE_ERROR) {
                    NSLog(@"Error when counting rows %s", sqlite3_errmsg(_local_db));
                } else {
                    rows = sqlite3_column_int(stmt, 0);
                }
            }
            
            if (rows == 0) {
                char * error;
                
                NSString * insertSQL = [NSString stringWithFormat:@"insert into practice values('%@', '%@', '%@')", gStrEmail, gStrUsername, gStrCode];
                sqlite3_exec(_local_db, [insertSQL UTF8String], NULL, 0, &error);
            }
        }
        else {
            NSLog( @"Failed to open database" );
        }
        
        [self performSegueWithIdentifier:@"showResultView" sender:nil];
    }
    else
    {
        _lblMessage.text = @"Wrong GUID code. Please check your email again";
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

@end
