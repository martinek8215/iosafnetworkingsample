//
//  CTConfig.h
//  iOS_WebService
//
//  Created by Adrian on 11/10/15.
//  Copyright (c) 2015 Adrian. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma once

// Web Service URL Definition


#define WEBAPI_URL                      @"YOUR_URL"

// Controller Name Definition
#define WEBAPI_ACCOUNT_CONTROLLER       @"accounts"

// Action Name Definition - User Section
#define WEBAPI_USER_SIGNUP              @"sign_up"
#define WEBAPI_USER_SIGNIN              @"sign_in"


// Return Values From Web Service
#define ERROR_UPDATE_USER               -4
#define ERROR_SEND_GUIDCODE             -3
#define ERROR_SEND_VERIFICATIONCODE     -2
#define ERROR_CREATE_USER               -1
#define ERROR_LOGIN                     0
#define SUCCESS_LOGIN                   1
#define SUCCESS_SEND_VERIFICATIONCODE   2
#define SUCCESS_SEND_GUIDCODE           3

#define IS_OS_8_OR_LATER                ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define RGBHEX(rgbValue,a)          [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

NSString * gStrEmail;
NSString * gStrUsername;
NSString * gStrCode;

/////////////////////////////////////////////////////////