//
//  NetAPIClient.h
//
//  Created by Adrian on 11/10/15.
//  Copyright (c) 2015 Adrian. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFNetworking.h"
#import "CTConfig.h"

typedef enum {
    MediaTypePhoto = 0,
    MediaTypeVideo,
    MediaTypeAudio
} MediaType;

@interface NetAPIClient : AFHTTPSessionManager

+ (NetAPIClient *)sharedClient;

// send text data
- (void)sendToServiceByPOST:(NSString *)serviceAPIURL
                 controller:(NSString *)_controller
                     action:(NSString *)_action
               params:(NSDictionary*)_params
              success:(void (^)(id _responseObject))_success
              failure:(void (^)(NSError* _error))_failure;

//send photo/video data
- (void)sendToServiceByPOST:(NSString *)serviceAPIURL
                     params:(NSDictionary *)_params
                      media:(NSData* )_media
                  mediaType:(MediaType)_mediaType // 0: photo, 1: video 2: audio
                    success:(void (^)(id _responseObject))_success
                    failure:(void (^)(NSError* _error))_failure
                   progress:(void(^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))_progress;

// get text data
- (void)sendToServiceByGET:(NSString *)serviceAPIURL
                    params:(NSDictionary* )_params
                   success:(void (^)(id _responseObject))_success
                   failure:(void (^)(NSError* _error))_failure;
@end
